#include "stdafx.h"
#include "main.h"
#include <stdlib.h>
#include <Windows.h>
#include <C:/GLUT/include/GL/glut.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include "ImageLoader.h"
#define Pi 3.14159

bool estado = false;
//float movX=0, movY=0, movZ=10,centerX=0,centerY=0,centerZ=0,upX=0,upY=1,upZ=0;
float movX = -25, movY = 85, movZ = -25, centerX = -25, centerY = 0, centerZ = -25, upX = 0, upY = 0, upZ = -1;
//float movX = -40, movY = -17, movZ = 1, centerX = -60, centerY = -17, centerZ = 1, upX = 0, upY = 0, upZ = 1;

float ax, az;
float av = 0;
int contador = 0;
int pisoCoord[1300][1300];
int coordenada = 0;
int coordenada2 = 0;
int coordenada3 = 0, coordenada4 = 0;

GLuint _muro;
GLuint _piso;
GLuint _cubo;
GLuint _cubonorm;
GLuint _tubo;
GLuint _castillo;
GLuint _bandera;
GLuint _enemigo;



// carga de las texturas 

using namespace std;
GLuint loadTexture(Image* image) {
	GLuint idtextura;
	glGenTextures(1, &idtextura);
	glBindTexture(GL_TEXTURE_2D, idtextura);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	return idtextura;
}

// llamado de la texturas 
void initRendering() {
	Image* muro = loadBMP("3.bmp");
	_muro = loadTexture(muro);
	Image* piso = loadBMP("2.bmp");
	_piso = loadTexture(piso);
	Image* cubo = loadBMP("A.bmp");
	_cubo = loadTexture(cubo);
	Image* cubonorm = loadBMP("ladrillo.bmp");
	_cubonorm = loadTexture(cubonorm);
	Image* tubo = loadBMP("ladrillo.bmp");
	_tubo = loadTexture(tubo);
	Image* castillo = loadBMP("ladrillo.bmp");
	_castillo = loadTexture(castillo);
	Image* bandera = loadBMP("ladrillo.bmp");
	_bandera = loadTexture(bandera);
	Image* enemigo = loadBMP("ladrillo.bmp");
	_enemigo = loadTexture(enemigo);




	delete muro, piso, cubo, cubonorm, tubo, castillo, bandera;
}


void llenarMatriz() {
	for (int i = 0; i < 650; i++) {
		for (int j = 0; j < 800; j++) {
			pisoCoord[i][j] = 0;
		}
	}
}

// moviemiento de la teclas 
void Arrowkeys(int key, int x, int y) {
	if (estado == false) {
		switch (key) {
		case GLUT_KEY_UP:
			movZ--;
			centerZ--;
			break;
		case GLUT_KEY_DOWN:
			movZ++;
			centerZ++;
			break;
		case GLUT_KEY_LEFT:
			movX--;
			centerX--;
			break;
		case GLUT_KEY_RIGHT:
			movX++;
			centerX++;
			break;
		}
	}
	else {
		switch (key) {
		case GLUT_KEY_UP:
			ax = movX + 0.1 * sin(av);
			az = movZ - 0.1 * cos(av);
			coordenada = pisoCoord[(int)(ax * 10 + 600)][int(az * 10 + 400)];
			ax = movX + 0.3 * sin(av);
			az = movZ - 0.3 * cos(av);
			coordenada3 = pisoCoord[(int)(ax * 10 + 600)][int(az * 10 + 400)];
			ax = movX + 0.1 * sin(av);
			az = movZ - 0.1 * cos(av);
			//cout << (ax * 10 + 600) <<","<< (az * 10 + 400) << endl;
			if (coordenada == 0) {
				movX = ax;
				movZ = az;
				movY = 0.35;
				centerY = 0.35;
				centerX = movX + 1 * sin(av);
				centerZ = movZ - 1 * cos(av);
			}
			else if (coordenada <= 3 && coordenada3 <= 3) {
				movX = ax;
				movZ = az;
				movY = 0.55;
				centerY = 0.55;
				centerX = movX + 1 * sin(av);
				centerZ = movZ - 1 * cos(av);
			}
			else {
				cout << "Colision detectada" << endl;
			}
			break;
		case GLUT_KEY_DOWN:
			ax = movX - 0.1 * sin(av);
			az = movZ + 0.1 * cos(av);
			coordenada = pisoCoord[(int)(ax * 10 + 600)][int(az * 10 + 400)];
			ax = movX - 0.3 * sin(av);
			az = movZ + 0.3 * cos(av);
			coordenada3 = pisoCoord[(int)(ax * 10 + 600)][int(az * 10 + 400)];
			ax = movX - 0.1 * sin(av);
			az = movZ + 0.1 * cos(av);
			//cout << (ax * 10 + 600) <<","<< (az * 10 + 400) << endl;
			if (coordenada == 0) {
				movX = ax;
				movZ = az;
				movY = 0.35;
				centerY = 0.35;
				centerX = movX + 1 * sin(av);
				centerZ = movZ - 1 * cos(av);
			}
			else if (coordenada <= 3 && coordenada3 <= 3) {
				movX = ax;
				movZ = az;
				movY = 0.55;
				centerY = 0.55;
				centerX = movX + 1 * sin(av);
				centerZ = movZ - 1 * cos(av);
			}
			else {
				cout << "Colision detectada" << endl;
			}
			break;
		case GLUT_KEY_LEFT:
			av = av - (Pi / 70);
			centerX = movX + 1 * sin(av);
			centerZ = movZ - 1 * cos(av);
			break;
		case GLUT_KEY_RIGHT:
			av = av + (Pi / 70);
			centerX = movX + 1 * sin(av);
			centerZ = movZ - 1 * cos(av);
			break;
		}
	}
	glutPostRedisplay();

}

// creacion de de las estructuras 

void linea(int x1, int y1, int x2, int y2, int alto) {
	int dx = x2 - x1, dy = y2 - y1, p;
	int adx = abs(dx), ady = abs(dy);

	pisoCoord[x1][y1] = alto;
	if (dx>0) {
		dx = 1;
	}
	else if (dx<0) {
		dx = -1;
	}
	else {
		dx = 0;
	}
	if (dy>0) {
		dy = 1;
	}
	else if (dy<0) {
		dy = -1;
	}
	else {
		dy = 0;
	}
	if (adx >= ady) {
		p = 2 * ady - adx;
		while (x1 != x2) {
			if (p<0) {
				x1 = x1 + dx;
				p = p + 2 * ady;
			}
			else {
				x1 = x1 + dx;
				y1 = y1 + dy;
				p = p + 2 * (ady - adx);
			}
			//cout << x1 << "," << y1 << endl;
			pisoCoord[x1][y1] = alto;
		}
	}
	else if (ady>adx) {
		p = 2 * adx - ady;
		while (y1 != y2) {
			if (p < 0) {
				y1 = y1 + dy;
				p = p + 2 * adx;
			}
			else {
				y1 = y1 + ady;
				x1 = x1 + adx;
				p = p + 2 * (adx - ady);
			}
			//cout << x1 << "," << y1 << endl;
			pisoCoord[x1][y1] = alto;
		}
	}
	//cout << x2 << "," << y2 << endl;
	pisoCoord[x2][y2] = alto;
}


// acercamiento y alejamiento de la camara 
void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case '+':
		if (movY > 10) {
			movY--;
		}
		break;
	case '-':
		if (movY<500) {
			movY++;
		}
		break;
	}
	glutPostRedisplay();
}



//movimiento del mouse para las camaras 

void onMouse(int button, int state, int x, int y) {
	switch (button) {
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN) {
			if (estado == false) {
				estado = true;
				movX = -25, movY = 0.35, movZ = -25, centerX = -100, centerY = 0.35, centerZ = 0, upX = 0, upY = 1, upZ = 0;
			}
			else {
				estado = false;
				movX = -25, movY = 85, movZ = -25, centerX = -25, centerY = 0, centerZ = -25, upX = 0, upY = 0, upZ = -1;
			}
		}
						  break;
	}
}









void init(void) {
	glClearColor(0.0, 0.0, 0.0, 0.1);

	glDepthRange(-1, 1);
}


void reshape(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(20, w / h, 0.001, 1000);
	glMatrixMode(GL_MODELVIEW);

}

void piso() {
	//piso
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _piso);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glColor3b(92, 51, 0);
	glBegin(GL_POLYGON);
	glTexCoord3f(-80, -40, 0);
	glVertex3f(-80, -40, 0);
	glTexCoord3f(400, -40, 0);
	glVertex3f(400, -40, 0);
	glTexCoord3f(400, 40, 0);
	glVertex3f(400, 40, 0);
	glTexCoord3f(-80, 40, 0);
	glVertex3f(-80, 40, 0);
	glEnd();
}

void prisma(GLfloat x, GLfloat y, GLfloat z, GLfloat base, GLfloat altura, GLfloat ancho) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _muro);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z + ancho, y);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x + base, z + ancho, y);
	glVertex3f(x + base, y, z + ancho);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();


	
	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x);
	glVertex3f(x, y, z);
	glTexCoord3f(z, y + altura, x);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x);
	glVertex3f(x, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, y, z + ancho);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x, y + altura, z + ancho);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, y + altura, z + ancho);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, y, z + ancho);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x, z + ancho, y + altura);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, z + ancho, y + altura);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x + base);
	glVertex3f(x + base, y, z);
	glTexCoord3f(z, y + altura, x + base);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x + base);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x + base);
	glVertex3f(x + base, y, z + ancho);
	glEnd();


	if (contador == 0)
	{
		if (y < 0) {
			y = abs(y);
			cout << y << endl;
			linea(x * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, y * 10 + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, ((y - altura) * 10) + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, (x + base) * 10 + 600, (y * 10) + 400, (z + ancho) * 10);

		}
		else {
			cout << y << endl;
			linea(x * 10 + 600, (400 - (y + altura) * 10), x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - y * 10, x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), x * 10 + 600, 400 - ((y + altura) * 10), (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), (x + base) * 10 + 600, 400 - (y * 10), (z + ancho) * 10);
		}
	}
}

void cubosorpresa(GLfloat x, GLfloat y, GLfloat z, GLfloat base, GLfloat altura, GLfloat ancho) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _cubo);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z + ancho, y);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x + base, z + ancho, y);
	glVertex3f(x + base, y, z + ancho);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();


	
	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x);
	glVertex3f(x, y, z);
	glTexCoord3f(z, y + altura, x);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x);
	glVertex3f(x, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, y, z + ancho);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x, y + altura, z + ancho);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, y + altura, z + ancho);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, y, z + ancho);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x, z + ancho, y + altura);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, z + ancho, y + altura);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x + base);
	glVertex3f(x + base, y, z);
	glTexCoord3f(z, y + altura, x + base);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x + base);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x + base);
	glVertex3f(x + base, y, z + ancho);
	glEnd();


	if (contador == 0)
	{
		if (y < 0) {
			y = abs(y);
			cout << y << endl;
			linea(x * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, y * 10 + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, ((y - altura) * 10) + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, (x + base) * 10 + 600, (y * 10) + 400, (z + ancho) * 10);

		}
		else {
			cout << y << endl;
			linea(x * 10 + 600, (400 - (y + altura) * 10), x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - y * 10, x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), x * 10 + 600, 400 - ((y + altura) * 10), (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), (x + base) * 10 + 600, 400 - (y * 10), (z + ancho) * 10);
		}
	}
}

void cubonormal(GLfloat x, GLfloat y, GLfloat z, GLfloat base, GLfloat altura, GLfloat ancho) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _cubonorm);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z + ancho, y);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x + base, z + ancho, y);
	glVertex3f(x + base, y, z + ancho);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();


	
	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x);
	glVertex3f(x, y, z);
	glTexCoord3f(z, y + altura, x);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x);
	glVertex3f(x, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, y, z + ancho);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x, y + altura, z + ancho);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, y + altura, z + ancho);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, y, z + ancho);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x, z + ancho, y + altura);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, z + ancho, y + altura);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x + base);
	glVertex3f(x + base, y, z);
	glTexCoord3f(z, y + altura, x + base);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x + base);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x + base);
	glVertex3f(x + base, y, z + ancho);
	glEnd();


	if (contador == 0)
	{
		if (y < 0) {
			y = abs(y);
			cout << y << endl;
			linea(x * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, y * 10 + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, ((y - altura) * 10) + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, (x + base) * 10 + 600, (y * 10) + 400, (z + ancho) * 10);

		}
		else {
			cout << y << endl;
			linea(x * 10 + 600, (400 - (y + altura) * 10), x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - y * 10, x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), x * 10 + 600, 400 - ((y + altura) * 10), (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), (x + base) * 10 + 600, 400 - (y * 10), (z + ancho) * 10);
		}
	}
}

void tubo(GLfloat x, GLfloat y, GLfloat z, GLfloat base, GLfloat altura, GLfloat ancho) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _tubo);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z + ancho, y);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x + base, z + ancho, y);
	glVertex3f(x + base, y, z + ancho);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();


	
	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x);
	glVertex3f(x, y, z);
	glTexCoord3f(z, y + altura, x);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x);
	glVertex3f(x, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, y, z + ancho);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x, y + altura, z + ancho);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, y + altura, z + ancho);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, y, z + ancho);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x, z + ancho, y + altura);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, z + ancho, y + altura);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x + base);
	glVertex3f(x + base, y, z);
	glTexCoord3f(z, y + altura, x + base);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x + base);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x + base);
	glVertex3f(x + base, y, z + ancho);
	glEnd();


	if (contador == 0)
	{
		if (y < 0) {
			y = abs(y);
			cout << y << endl;
			linea(x * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, y * 10 + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, ((y - altura) * 10) + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, (x + base) * 10 + 600, (y * 10) + 400, (z + ancho) * 10);

		}
		else {
			cout << y << endl;
			linea(x * 10 + 600, (400 - (y + altura) * 10), x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - y * 10, x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), x * 10 + 600, 400 - ((y + altura) * 10), (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), (x + base) * 10 + 600, 400 - (y * 10), (z + ancho) * 10);
		}
	}
}

void castillo(GLfloat x, GLfloat y, GLfloat z, GLfloat base, GLfloat altura, GLfloat ancho) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _castillo);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z + ancho, y);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x + base, z + ancho, y);
	glVertex3f(x + base, y, z + ancho);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();


	
	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x);
	glVertex3f(x, y, z);
	glTexCoord3f(z, y + altura, x);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x);
	glVertex3f(x, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, y, z + ancho);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x, y + altura, z + ancho);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, y + altura, z + ancho);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, y, z + ancho);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x, z + ancho, y + altura);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, z + ancho, y + altura);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x + base);
	glVertex3f(x + base, y, z);
	glTexCoord3f(z, y + altura, x + base);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x + base);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x + base);
	glVertex3f(x + base, y, z + ancho);
	glEnd();


	if (contador == 0)
	{
		if (y < 0) {
			y = abs(y);
			cout << y << endl;
			linea(x * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, y * 10 + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, ((y - altura) * 10) + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, (x + base) * 10 + 600, (y * 10) + 400, (z + ancho) * 10);

		}
		else {
			cout << y << endl;
			linea(x * 10 + 600, (400 - (y + altura) * 10), x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - y * 10, x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), x * 10 + 600, 400 - ((y + altura) * 10), (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), (x + base) * 10 + 600, 400 - (y * 10), (z + ancho) * 10);
		}
	}
}

void bandera(GLfloat x, GLfloat y, GLfloat z, GLfloat base, GLfloat altura, GLfloat ancho) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _bandera);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z + ancho, y);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x + base, z + ancho, y);
	glVertex3f(x + base, y, z + ancho);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();


	
	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x);
	glVertex3f(x, y, z);
	glTexCoord3f(z, y + altura, x);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x);
	glVertex3f(x, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, y, z + ancho);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x, y + altura, z + ancho);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, y + altura, z + ancho);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, y, z + ancho);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x, z + ancho, y + altura);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, z + ancho, y + altura);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x + base);
	glVertex3f(x + base, y, z);
	glTexCoord3f(z, y + altura, x + base);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x + base);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x + base);
	glVertex3f(x + base, y, z + ancho);
	glEnd();


	if (contador == 0)
	{
		if (y < 0) {
			y = abs(y);
			cout << y << endl;
			linea(x * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, y * 10 + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, ((y - altura) * 10) + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, (x + base) * 10 + 600, (y * 10) + 400, (z + ancho) * 10);

		}
		else {
			cout << y << endl;
			linea(x * 10 + 600, (400 - (y + altura) * 10), x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - y * 10, x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), x * 10 + 600, 400 - ((y + altura) * 10), (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), (x + base) * 10 + 600, 400 - (y * 10), (z + ancho) * 10);
		}
	}
}

void enemigo(GLfloat x, GLfloat y, GLfloat z, GLfloat base, GLfloat altura, GLfloat ancho) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _enemigo);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z + ancho, y);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x + base, z + ancho, y);
	glVertex3f(x + base, y, z + ancho);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();


	
	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x);
	glVertex3f(x, y, z);
	glTexCoord3f(z, y + altura, x);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x);
	glVertex3f(x, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, y, z + ancho);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x, y + altura, z + ancho);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, y + altura, z + ancho);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, y, z + ancho);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x, z + ancho, y + altura);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, z + ancho, y + altura);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x + base);
	glVertex3f(x + base, y, z);
	glTexCoord3f(z, y + altura, x + base);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x + base);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x + base);
	glVertex3f(x + base, y, z + ancho);
	glEnd();


	if (contador == 0)
	{
		if (y < 0) {
			y = abs(y);
			cout << y << endl;
			linea(x * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, y * 10 + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, ((y - altura) * 10) + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, (x + base) * 10 + 600, (y * 10) + 400, (z + ancho) * 10);

		}
		else {
			cout << y << endl;
			linea(x * 10 + 600, (400 - (y + altura) * 10), x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - y * 10, x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), x * 10 + 600, 400 - ((y + altura) * 10), (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), (x + base) * 10 + 600, 400 - (y * 10), (z + ancho) * 10);
		}
	}
}

void display() {


	glMatrixMode(GL_MODELVIEW);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.3, 0.6, 0.9, 0.9);

	//glPushMatrix();
	//glRotatef(45, 1, 0, 0);
	glLoadIdentity();
	//gluLookAt(-15,0.35,25,0,0,0,0,1,0);
	gluLookAt(movX, movY, movZ, centerX, centerY, centerZ, upX, upY, upZ);
	//gluLookAt(-50,-50, 10, 0, 0, 10, 0, 0, 1);
	//gluLookAt(50, -50, 10, 0, 0, 10, 0, 0, 1);
	//gluLookAt(50, 50, 10, 0, 0, 10, 0, 0, 1);
	//gluLookAt(-50, 50, 10, 0, 0, 10, 0, 0, 1);
	//gluLookAt(movX, movY, movZ, centerX, centerY, centerZ, upX, upY, upZ);




	glPushMatrix();
	glRotatef(-90, 1, 0, 0);
	glColor3d(254, 0, 0);
	glPushMatrix();
	glTranslatef(-19.4, 13.0, 0.2);
	glutSolidSphere(1.5, 100, 100);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-16.2, 13.0, 0.2);
	glutSolidSphere(1.5, 100, 100);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-13.0, 13.0, 0.2);
	glutSolidSphere(1.5, 100, 100);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-10.2, 13.0, 0.2);
	glutSolidSphere(1.2, 100, 100);
	glPopMatrix();

	piso();

	

	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	//PISO 1
	prisma(-20, 5, 0, .5, .5, .5);  //x,y,altura,largo,ancho,altura
	prisma(-19.5, 5, 0, .5, .5, .5);
	prisma(-19, 5, 0, .5, .5, .5);
	prisma(-18.5, 5, 0, .5, .5, .5);
	prisma(-18, 5, 0, .5, .5, .5);
	prisma(-17.5, 5, 0, .5, .5, .5);
	prisma(-17, 5, 0, .5, .5, .5);
	prisma(-16.5, 5, 0, .5, .5, .5);
	prisma(-16, 5, 0, .5, .5, .5);
	prisma(-15.5, 5, 0, .5, .5, .5);
	prisma(-15, 5, 0, .5, .5, .5);
	prisma(-14.5, 5, 0, .5, .5, .5);
	prisma(-14, 5, 0, .5, .5, .5);
	prisma(-13.5, 5, 0, .5, .5, .5);
	prisma(-13, 5, 0, .5, .5, .5);
	prisma(-12.5, 5, 0, .5, .5, .5);
	prisma(-12, 5, 0, .5, .5, .5);
	prisma(-19.5, 5, 0, .5, .5, .5);
	prisma(-19, 5, 0, .5, .5, .5);
	prisma(-18.5, 5, 0, .5, .5, .5);
	prisma(-18, 5, 0, .5, .5, .5);
	prisma(-17.5, 5, 0, .5, .5, .5);
	prisma(-17, 5, 0, .5, .5, .5);
	prisma(-16.5, 5, 0, .5, .5, .5);
	prisma(-16, 5, 0, .5, .5, .5);
	prisma(-15.5, 5, 0, .5, .5, .5);
	prisma(-15, 5, 0, .5, .5, .5);
	prisma(-14.5, 5, 0, .5, .5, .5);
	prisma(-14, 5, 0, .5, .5, .5);
	prisma(-13.5, 5, 0, .5, .5, .5);
	prisma(-13, 5, 0, .5, .5, .5);
	prisma(-12.5, 5, 0, .5, .5, .5);
	prisma(-12, 5, 0, .5, .5, .5);
	prisma(-11.5, 5, 0, .5, .5, .5);
	prisma(-11, 5, 0, .5, .5, .5);
	prisma(-10.5, 5, 0, .5, .5, .5);
	prisma(-10, 5, 0, .5, .5, .5);
	prisma(-9.5, 5, 0, .5, .5, .5);
	prisma(-9, 5, 0, .5, .5, .5);
	prisma(-8.5, 5, 0, .5, .5, .5);
	prisma(-8, 5, 0, .5, .5, .5);
	prisma(-7.5, 5, 0, .5, .5, .5);
	prisma(-7, 5, 0, .5, .5, .5);
	prisma(-6.5, 5, 0, .5, .5, .5);
	prisma(-6, 5, 0, .5, .5, .5);
	prisma(-5.5, 5, 0, .5, .5, .5);
	prisma(-5, 5, 0, .5, .5, .5);
	prisma(-4.5, 5, 0, .5, .5, .5);
	prisma(-4, 5, 0, .5, .5, .5);
	prisma(-3.5, 5, 0, .5, .5, .5);
	prisma(-3, 5, 0, .5, .5, .5);
	prisma(-2.5, 5, 0, .5, .5, .5);
	prisma(-2, 5, 0, .5, .5, .5);
	prisma(-1.5, 5, 0, .5, .5, .5);
	prisma(-1, 5, 0, .5, .5, .5);
	prisma(-.5, 5, 0, .5, .5, .5);
	prisma(0, 5, 0, .5, .5, .5);
	prisma(.5, 5, 0, .5, .5, .5);
	prisma(1, 5, 0, .5, .5, .5);
	prisma(1.5, 5, 0, .5, .5, .5);
	prisma(2, 5, 0, .5, .5, .5);
	prisma(2.5, 5, 0, .5, .5, .5);
	prisma(3, 5, 0, .5, .5, .5);
	prisma(3.5, 5, 0, .5, .5, .5);
	prisma(4, 5, 0, .5, .5, .5);
	prisma(4.5, 5, 0, .5, .5, .5);
	prisma(5, 5, 0, .5, .5, .5);
	prisma(5.5, 5, 0, .5, .5, .5);
	prisma(6, 5, 0, .5, .5, .5);
	prisma(6.5, 5, 0, .5, .5, .5);
	prisma(7, 5, 0, .5, .5, .5);
	prisma(7.5, 5, 0, .5, .5, .5);
	//
	prisma(-20, 5, .5, .5, .5, .5);  //x,y,altura,largo,ancho,altura
	prisma(-19.5, 5, .5, .5, .5, .5);
	prisma(-19, 5, .5, .5, .5, .5);
	prisma(-18.5, 5,.5, .5, .5, .5);
	prisma(-18, 5, .5, .5, .5, .5);
	prisma(-17.5, 5, .5, .5, .5, .5);
	prisma(-17, 5, .5, .5, .5, .5);
	prisma(-16.5, 5, .5, .5, .5, .5);
	prisma(-16, 5, .5, .5, .5, .5);
	prisma(-15.5, 5, .5, .5, .5, .5);
	prisma(-15, 5, .5, .5, .5, .5);
	prisma(-14.5, 5, .5, .5, .5, .5);
	prisma(-14, 5, .5, .5, .5, .5);
	prisma(-13.5, 5, .5, .5, .5, .5);
	prisma(-13, 5, .5, .5, .5, .5);
	prisma(-12.5, 5, .5, .5, .5, .5);
	prisma(-12, 5, .5, .5, .5, .5);
	prisma(-19.5, 5, .5, .5, .5, .5);
	prisma(-19, 5, .5, .5, .5, .5);
	prisma(-18.5, 5, .5, .5, .5, .5);
	prisma(-18, 5, .5, .5, .5, .5);
	prisma(-17.5, 5, .5, .5, .5, .5);
	prisma(-17, 5, .5, .5, .5, .5);
	prisma(-16.5, 5, .5, .5, .5, .5);
	prisma(-16, 5, .5, .5, .5, .5);
	prisma(-15.5, 5, .5, .5, .5, .5);
	prisma(-15, 5, .5, .5, .5, .5);
	prisma(-14.5, 5, .5, .5, .5, .5);
	prisma(-14, 5, .5, .5, .5, .5);
	prisma(-13.5, 5, .5, .5, .5, .5);
	prisma(-13, 5, .5, .5, .5, .5);
	prisma(-12.5, 5, .5, .5, .5, .5);
	prisma(-12, 5, .5, .5, .5, .5);
	prisma(-11.5, 5, .5, .5, .5, .5);
	prisma(-11, 5, .5, .5, .5, .5);
	prisma(-10.5, 5, .5, .5, .5, .5);
	prisma(-10, 5, .5, .5, .5, .5);
	prisma(-9.5, 5, .5, .5, .5, .5);
	prisma(-9, 5, .5, .5, .5, .5);
	prisma(-8.5, 5, .5, .5, .5, .5);
	prisma(-8, 5, .5, .5, .5, .5);
	prisma(-7.5, 5, .5, .5, .5, .5);
	prisma(-7, 5, .5, .5, .5, .5);
	prisma(-6.5, 5, .5, .5, .5, .5);
	prisma(-6, 5, .5, .5, .5, .5);
	prisma(-5.5, 5, .5, .5, .5, .5);
	prisma(-5, 5, .5, .5, .5, .5);
	prisma(-4.5, 5, .5, .5, .5, .5);
	prisma(-4, 5, .5, .5, .5, .5);
	prisma(-3.5, 5, .5, .5, .5, .5);
	prisma(-3, 5, .5, .5, .5, .5);
	prisma(-2.5, 5, .5, .5, .5, .5);
	prisma(-2, 5, .5, .5, .5, .5);
	prisma(-1.5, 5, .5, .5, .5, .5);
	prisma(-1, 5, .5, .5, .5, .5);
	prisma(-.5, 5, .5, .5, .5, .5);
	prisma(0, 5, .5, .5, .5, .5);
	prisma(.5, 5, .5, .5, .5, .5);
	prisma(1, 5, .5, .5, .5, .5);
	prisma(1.5, 5, .5, .5, .5, .5);
	prisma(2, 5, .5, .5, .5, .5);
	prisma(2.5, 5, .5, .5, .5, .5);
	prisma(3, 5, .5, .5, .5, .5);
	prisma(3.5, 5, .5, .5, .5, .5);
	prisma(4, 5, .5, .5, .5, .5);
	prisma(4.5, 5, .5, .5, .5, .5);
	prisma(5, 5, .5, .5, .5, .5);
	prisma(5.5, 5, .5, .5, .5, .5);
	prisma(6, 5, .5, .5, .5, .5);
	prisma(6.5, 5, .5, .5, .5, .5);
	prisma(7, 5, .5, .5, .5, .5);
	prisma(7.5, 5, .5, .5, .5, .5);
	//PRIMER CUBO SORPRESA
	cubosorpresa(-12, 5, 2.5, .5, .5, .5);
	cubosorpresa(-10, 5, 2.5, .5, .5, .5);
	cubosorpresa(-9, 5, 2.5, .5, .5, .5);
	cubosorpresa(4.5, 5, 4, .5, .5, .5);
	//PRIMER CUBO NOROMAL
	cubonormal(-10.5, 5, 2.5, .5, .5, .5);
	cubonormal(-9.5, 5, 2.5, .5, .5, .5);
	cubonormal(-8.5, 5, 2.5, .5, .5, .5);
	cubonormal(-9.5, 5, 5, .5, .5, .5);
	//primer tubo
	tubo(-6.5, 5, 1, 1, .5, 1);
	//segundo tubo
	tubo(-2, 5, 1, 1, .5, 1.5);
	//tercer tubo
	tubo(2, 5, 1, 1, .5, 2);

	//SEGUNDA PARTE

	//ARRIBA
	prisma(9, 5, .5, .5, .5, .5);
	prisma(9.5, 5, .5, .5, .5, .5);
	prisma(10, 5, .5, .5, .5, .5);
	prisma(10.5, 5, .5, .5, .5, .5);
	prisma(11, 5, .5, .5, .5, .5);
	prisma(11.5, 5, .5, .5, .5, .5);
	prisma(12, 5, .5, .5, .5, .5);
	prisma(12.5, 5, .5, .5, .5, .5);
	prisma(13, 5, .5, .5, .5, .5);
	prisma(13.5, 5, .5, .5, .5, .5);
	prisma(14, 5, .5, .5, .5, .5);
	prisma(14.5, 5, .5, .5, .5, .5);
	prisma(15, 5, .5, .5, .5, .5);
	prisma(15.5, 5, .5, .5, .5, .5);
	prisma(16, 5, .5, .5, .5, .5);
	//ABAJO
	prisma(9, 5, 0, .5, .5, .5);
	prisma(9.5, 5, 0, .5, .5, .5);
	prisma(10, 5, 0, .5, .5, .5);
	prisma(10.5, 5, 0, .5, .5, .5);
	prisma(11, 5, 0, .5, .5, .5);
	prisma(11.5, 5, 0, .5, .5, .5);
	prisma(12, 5, 0, .5, .5, .5);
	prisma(12.5, 5, 0, .5, .5, .5);
	prisma(13, 5, 0, .5, .5, .5);
	prisma(13.5, 5, 0, .5, .5, .5);
	prisma(14, 5, 0, .5, .5, .5);
	prisma(14.5, 5, 0, .5, .5, .5);
	prisma(15, 5, 0, .5, .5, .5);
	prisma(15.5, 5, 0, .5, .5, .5);
	prisma(16, 5, 0, .5, .5, .5);
	//PRIMER CUBO NOROMAL
	cubonormal(11.5, 5, 2.5, .5, .5, .5);
	cubonormal(12.5, 5, 2.5, .5, .5, .5);
	cubonormal(13, 5, 4, .5, .5, .5);
	cubonormal(13.5, 5, 4, .5, .5, .5);
	cubonormal(14, 5, 4, .5, .5, .5);
	cubonormal(14.5, 5, 4, .5, .5, .5);
	cubonormal(15, 5, 4, .5, .5, .5);
	cubonormal(15.5, 5, 4, .5, .5, .5);
	cubonormal(16, 5, 4, .5, .5, .5);
	cubonormal(16.5, 5, 4, .5, .5, .5);
	cubonormal(17, 5, 4, .5, .5, .5);
	//PRIMER CUBO SORPRESA
	cubosorpresa(12, 5, 2.5, .5, .5, .5);
	//PISO TERCERA PARTE
	prisma(18.5, 5, 0, .5, .5, .5);
	prisma(19, 5, 0, .5, .5, .5);
	prisma(19.5, 5, 0, .5, .5, .5);
	prisma(20, 5, 0, .5, .5, .5);
	prisma(20.5, 5, 0, .5, .5, .5);
	prisma(21, 5, 0, .5, .5, .5);
	prisma(21.5, 5, 0, .5, .5, .5);
	prisma(22, 5, 0, .5, .5, .5);
	prisma(22.5, 5, 0, .5, .5, .5);
	prisma(23, 5, 0, .5, .5, .5);
	prisma(23.5, 5, 0, .5, .5, .5);
	prisma(24, 5, 0, .5, .5, .5);
	prisma(24.5, 5, 0, .5, .5, .5);
	prisma(25, 5, 0, .5, .5, .5);
	prisma(25.5, 5, 0, .5, .5, .5);
	prisma(26, 5, 0, .5, .5, .5);
	prisma(26.5, 5, 0, .5, .5, .5);
	prisma(27, 5, 0, .5, .5, .5);
	prisma(27.5, 5, 0, .5, .5, .5);
	prisma(28, 5, 0, .5, .5, .5);
	prisma(28.5, 5, 0, .5, .5, .5);
	prisma(29, 5, 0, .5, .5, .5);
	prisma(29.5, 5, 0, .5, .5, .5);
	prisma(30, 5, 0, .5, .5, .5);
	prisma(30.5, 5, 0, .5, .5, .5);
	prisma(31, 5, 0, .5, .5, .5);
	prisma(31.5, 5, 0, .5, .5, .5);
	prisma(32, 5, 0, .5, .5, .5);
	prisma(32.5, 5, 0, .5, .5, .5);
	prisma(33, 5, 0, .5, .5, .5);
	prisma(33.5, 5, 0, .5, .5, .5);
	prisma(34, 5, 0, .5, .5, .5);
	prisma(34.5, 5, 0, .5, .5, .5);
	prisma(35, 5, 0, .5, .5, .5);
	prisma(35.5, 5, 0, .5, .5, .5);
	prisma(36, 5, 0, .5, .5, .5);
	prisma(36.5, 5, 0, .5, .5, .5);
	prisma(37, 5, 0, .5, .5, .5);
	prisma(37.5, 5, 0, .5, .5, .5);
	prisma(38, 5, 0, .5, .5, .5);
	prisma(38.5, 5, 0, .5, .5, .5);
	prisma(39, 5, 0, .5, .5, .5);
	prisma(39.5, 5, 0, .5, .5, .5);
	prisma(40, 5, 0, .5, .5, .5);
	prisma(40.5, 5, 0, .5, .5, .5);
	prisma(41, 5, 0, .5, .5, .5);
	prisma(41.5, 5, 0, .5, .5, .5);
	prisma(42, 5, 0, .5, .5, .5);
	prisma(42.5, 5, 0, .5, .5, .5);
	prisma(43, 5, 0, .5, .5, .5);
	prisma(43.5, 5, 0, .5, .5, .5);
	prisma(44, 5, 0, .5, .5, .5);
	prisma(44.5, 5, 0, .5, .5, .5);
	prisma(45, 5, 0, .5, .5, .5);
	prisma(45.5, 5, 0, .5, .5, .5);
	prisma(46, 5, 0, .5, .5, .5);
	prisma(46.5, 5, 0, .5, .5, .5);
	prisma(47, 5, 0, .5, .5, .5);
	prisma(47.5, 5, 0, .5, .5, .5);
	prisma(48, 5, 0, .5, .5, .5);
	prisma(48.5, 5, 0, .5, .5, .5);
	prisma(49, 5, 0, .5, .5, .5);
	prisma(49.5, 5, 0, .5, .5, .5);
	prisma(50, 5, 0, .5, .5, .5);
	prisma(50.5, 5, 0, .5, .5, .5);
	prisma(51, 5, 0, .5, .5, .5);
	//ARRIBA
	prisma(18.5, 5, .5, .5, .5, .5);
	prisma(19, 5, .5, .5, .5, .5);
	prisma(19.5, 5, .5, .5, .5, .5);
	prisma(20, 5, .5, .5, .5, .5);
	prisma(20.5, 5, .5, .5, .5, .5);
	prisma(21, 5, .5, .5, .5, .5);
	prisma(21.5, 5, .5, .5, .5, .5);
	prisma(22, 5, .5, .5, .5, .5);
	prisma(22.5, 5, .5, .5, .5, .5);
	prisma(23, 5, .5, .5, .5, .5);
	prisma(23.5, 5, .5, .5, .5, .5);
	prisma(24, 5, .5, .5, .5, .5);
	prisma(24.5, 5, .5, .5, .5, .5);
	prisma(25, 5, .5, .5, .5, .5);
	prisma(25.5, 5, .5, .5, .5, .5);
	prisma(26, 5, .5, .5, .5, .5);
	prisma(26.5, 5, .5, .5, .5, .5);
	prisma(27, 5, .5, .5, .5, .5);
	prisma(27.5, 5, .5, .5, .5, .5);
	prisma(28, 5, .5, .5, .5, .5);
	prisma(28.5, 5, .5, .5, .5, .5);
	prisma(29, 5, .5, .5, .5, .5);
	prisma(29.5, 5, .5, .5, .5, .5);
	prisma(30, 5, .5, .5, .5, .5);
	prisma(30.5, 5, .5, .5, .5, .5);
	prisma(31, 5, .5, .5, .5, .5);
	prisma(31.5, 5, .5, .5, .5, .5);
	prisma(32, 5, .5, .5, .5, .5);
	prisma(32.5, 5, .5, .5, .5, .5);
	prisma(33, 5, .5, .5, .5, .5);
	prisma(33.5, 5, .5, .5, .5, .5);
	prisma(34, 5, .5, .5, .5, .5);
	prisma(34.5, 5, .5, .5, .5, .5);
	prisma(35, 5, .5, .5, .5, .5);
	prisma(35.5, 5, .5, .5, .5, .5);
	prisma(36, 5, .5, .5, .5, .5);
	prisma(36.5, 5, .5, .5, .5, .5);
	prisma(37, 5, .5, .5, .5, .5);
	prisma(37.5, 5, .5, .5, .5, .5);
	prisma(38, 5, .5, .5, .5, .5);
	prisma(38.5, 5, 0.5, .5, .5, .5);
	prisma(39, 5, 0.5, .5, .5, .5);
	prisma(39.5, 5, 0.5, .5, .5, .5);
	prisma(40, 5, 0.5, .5, .5, .5);
	prisma(40.5, 5, 0.5, .5, .5, .5);
	prisma(41, 5, 0.5, .5, .5, .5);
	prisma(41.5, 5, 0.5, .5, .5, .5);
	prisma(42, 5, 0.5, .5, .5, .5);
	prisma(42.5, 5, 0.5, .5, .5, .5);
	prisma(43, 5, 0.5, .5, .5, .5);
	prisma(43.5, 5, 0.5, .5, .5, .5);
	prisma(44, 5, 0.5, .5, .5, .5);
	prisma(44.5, 5, 0.5, .5, .5, .5);
	prisma(45, 5, 0.5, .5, .5, .5);
	prisma(45.5, 5, 0.5, .5, .5, .5);
	prisma(46, 5, 0.5, .5, .5, .5);
	prisma(46.5, 5, 0.5, .5, .5, .5);
	prisma(47, 5, 0.5, .5, .5, .5);
	prisma(47.5, 5, 0.5, .5, .5, .5);
	prisma(48, 5, 0.5, .5, .5, .5);
	prisma(48.5, 5, 0.5, .5, .5, .5);
	prisma(49, 5, 0.5, .5, .5, .5);
	prisma(49.5, 5, 0.5, .5, .5, .5);
	prisma(50, 5, 0.5, .5, .5, .5);
	prisma(50.5, 5, 0.5, .5, .5, .5);
	prisma(51, 5, 0.5, .5, .5, .5);
	//primer cubo
	cubonormal(19.5, 5, 4, .5, .5, .5);
	cubonormal(20, 5, 4, .5, .5, .5);
	cubonormal(20.5, 5, 4, .5, .5, .5);
	cubonormal(24, 5, 4, .5, .5, .5);
	cubonormal(34, 5, 2.5, .5, .5, .5);
	cubonormal(35.5, 5, 4, .5, .5, .5);
	cubonormal(36, 5, 4, .5, .5, .5);
	cubonormal(36.5, 5, 4, .5, .5, .5);
	cubonormal(40.5, 5, 2.5, .5, .5, .5);
	cubonormal(41, 5, 2.5, .5, .5, .5);
	cubonormal(40, 5, 4, .5, .5, .5);
	cubonormal(41.5, 5, 4, .5, .5, .5);
	//CUBO SORPRESA
	cubosorpresa(21, 5, 2.5, .5, .5, .5);
	cubosorpresa(21, 5, 2.5, .5, .5, .5);
	cubosorpresa(24.5, 5, 2.5, .5, .5, .5);
	cubosorpresa(28.5, 5, 2.5, .5, .5, .5);
	cubosorpresa(30, 5, 2.5, .5, .5, .5);
	cubosorpresa(31.5, 5, 2.5, .5, .5, .5);
	cubosorpresa(30, 5, 4, .5, .5, .5);
	cubosorpresa(40.5, 5, 4, .5, .5, .5);
	cubosorpresa(41, 5, 4, .5, .5, .5);
	//PRIMER ESCALERA
	cubonormal(43, 5, 1, .5, .5, .5);
	cubonormal(43.5, 5, 1, .5, .5, .5);
	cubonormal(44, 5, 1, .5, .5, .5);
	cubonormal(44.5, 5, 1, .5, .5, .5);
	cubonormal(43.5, 5, 1.5, .5, .5, .5);
	cubonormal(44, 5, 1.5, .5, .5, .5);
	cubonormal(44.5, 5, 1.5, .5, .5, .5);
	cubonormal(44, 5, 2, .5, .5, .5);
	cubonormal(44.5, 5, 2, .5, .5, .5);
	cubonormal(44.5, 5, 2.5, .5, .5, .5);
	//SEGUNDA ESCALERA
	cubonormal(46, 5, 1, .5, .5, .5);
	cubonormal(46.5, 5, 1, .5, .5, .5);
	cubonormal(47, 5, 1, .5, .5, .5);
	cubonormal(47.5, 5, 1, .5, .5, .5);
	cubonormal(46, 5, 1.5, .5, .5, .5);
	cubonormal(46.5, 5, 1.5, .5, .5, .5);
	cubonormal(47, 5, 1.5, .5, .5, .5);
	cubonormal(46, 5, 2, .5, .5, .5);
	cubonormal(46.5, 5, 2, .5, .5, .5);
	cubonormal(46, 5, 2.5, .5, .5, .5);
	
	//TERCERA ESCALERA
	cubonormal(49, 5, 1, .5, .5, .5);
	cubonormal(49.5, 5, 1, .5, .5, .5);
	cubonormal(50, 5, 1, .5, .5, .5);
	cubonormal(50.5, 5, 1, .5, .5, .5);
	cubonormal(51, 5, 1, .5, .5, .5);
	cubonormal(49.5, 5, 1.5, .5, .5, .5);
	cubonormal(50, 5, 1.5, .5, .5, .5);
	cubonormal(50.5, 5, 1.5, .5, .5, .5);
	cubonormal(51, 5, 1.5, .5, .5, .5);
	cubonormal(50, 5, 2, .5, .5, .5);
	cubonormal(50.5, 5, 2, .5, .5, .5);
	cubonormal(51, 5, 2, .5, .5, .5);
	cubonormal(50.5, 5, 2.5, .5, .5, .5);
	cubonormal(51, 5, 2.5, .5, .5, .5);
	//CUARTA PARTE 52.5
	prisma(52.5, 5, 0, .5, .5, .5);
	prisma(53, 5, 0, .5, .5, .5);
	prisma(53.5, 5, 0, .5, .5, .5);
	prisma(54, 5, 0, .5, .5, .5);
	prisma(54.5, 5, 0, .5, .5, .5);
	prisma(55, 5, 0, .5, .5, .5);
	prisma(55.5, 5, 0, .5, .5, .5);
	prisma(56, 5, 0, .5, .5, .5);
	prisma(56.5, 5, 0, .5, .5, .5);
	prisma(57, 5, 0, .5, .5, .5);
	prisma(57.5, 5, 0, .5, .5, .5);
	prisma(58, 5, 0, .5, .5, .5);
	prisma(58.5, 5, 0, .5, .5, .5);
	prisma(59, 5, 0, .5, .5, .5);
	prisma(59.5, 5, 0, .5, .5, .5);
	prisma(60, 5, 0, .5, .5, .5);
	prisma(60.5, 5, 0, .5, .5, .5);
	prisma(61, 5, 0, .5, .5, .5);
	prisma(61.5, 5, 0, .5, .5, .5);
	prisma(62, 5, 0, .5, .5, .5);
	prisma(62.5, 5, 0, .5, .5, .5);
	prisma(63, 5, 0, .5, .5, .5);
	prisma(63.5, 5, 0, .5, .5, .5);
	prisma(64, 5, 0, .5, .5, .5);
	prisma(64.5, 5, 0, .5, .5, .5);
	prisma(65, 5, 0, .5, .5, .5);
	prisma(65.5, 5, 0, .5, .5, .5);
	prisma(66, 5, 0, .5, .5, .5);
	prisma(66.5, 5, 0, .5, .5, .5);
	prisma(67, 5, 0, .5, .5, .5);
	prisma(67.5, 5, 0, .5, .5, .5);
	prisma(68, 5, 0, .5, .5, .5);
	prisma(68.5, 5, 0, .5, .5, .5);
	prisma(69, 5, 0, .5, .5, .5);
	prisma(69.5, 5, 0, .5, .5, .5);
	prisma(70, 5, 0, .5, .5, .5);
	prisma(70.5, 5, 0, .5, .5, .5);
	prisma(71, 5, 0, .5, .5, .5);
	prisma(71.5, 5, 0, .5, .5, .5);
	prisma(72, 5, 0, .5, .5, .5);
	prisma(72.5, 5, 0, .5, .5, .5);
	prisma(73, 5, 0, .5, .5, .5);
	prisma(73.5, 5, 0, .5, .5, .5);
	prisma(74, 5, 0, .5, .5, .5);
	prisma(74.5, 5, 0, .5, .5, .5);
	prisma(75, 5, 0, .5, .5, .5);
	prisma(75.5, 5, 0, .5, .5, .5);
	prisma(76, 5, 0, .5, .5, .5);
	prisma(76.5, 5, 0, .5, .5, .5);
	prisma(77.5, 5, 0, .5, .5, .5);
	prisma(77, 5, 0, .5, .5, .5);
	prisma(78.5, 5, 0, .5, .5, .5);
	prisma(78, 5, 0, .5, .5, .5);
	prisma(79.5, 5, 0, .5, .5, .5);
	prisma(79, 5, 0, .5, .5, .5);
	prisma(80, 5, 0, .5, .5, .5);
	prisma(80.5, 5, 0, .5, .5, .5);
	prisma(81, 5, 0, .5, .5, .5);
	prisma(81.5, 5, 0, .5, .5, .5);
	prisma(82, 5, 0, .5, .5, .5);
	prisma(82.5, 5, 0, .5, .5, .5);
	prisma(83, 5, 0, .5, .5, .5);
	prisma(83.5, 5, 0, .5, .5, .5);
	prisma(84, 5, 0, .5, .5, .5);
	prisma(84.5, 5, 0, .5, .5, .5);
	prisma(85, 5, 0, .5, .5, .5);
	prisma(85.5, 5, 0, .5, .5, .5);
	prisma(86, 5, 0, .5, .5, .5);
	prisma(86.5, 5, 0, .5, .5, .5);
	prisma(87, 5, 0, .5, .5, .5);
	prisma(87.5, 5, 0, .5, .5, .5);
	prisma(88, 5, 0, .5, .5, .5);
	prisma(88.5, 5, 0, .5, .5, .5);
	prisma(89, 5, 0, .5, .5, .5);
	prisma(89.5, 5, 0, .5, .5, .5);
	//ARRIBA
	prisma(52.5, 5, 0.5, .5, .5, .5);
	prisma(53, 5, 0.5, .5, .5, .5);
	prisma(53.5, 5, 0.5, .5, .5, .5);
	prisma(54, 5, 0.5, .5, .5, .5);
	prisma(54.5, 5, 0.5, .5, .5, .5);
	prisma(55, 5, 0.5, .5, .5, .5);
	prisma(55.5, 5, 0.5, .5, .5, .5);
	prisma(56, 5, 0.5, .5, .5, .5);
	prisma(56.5, 5, 0.5, .5, .5, .5);
	prisma(57, 5, 0.5, .5, .5, .5);
	prisma(57.5, 5, 0.5, .5, .5, .5);
	prisma(58, 5, 0.5, .5, .5, .5);
	prisma(58.5, 5, 0.5, .5, .5, .5);
	prisma(59, 5, 0.5, .5, .5, .5);
	prisma(59.5, 5, 0.5, .5, .5, .5);
	prisma(60, 5, 0.5, .5, .5, .5);
	prisma(60.5, 5, 0.5, .5, .5, .5);
	prisma(61, 5, 0.5, .5, .5, .5);
	prisma(61.5, 5, 0.5, .5, .5, .5);
	prisma(62, 5, 0.5, .5, .5, .5);
	prisma(62.5, 5, 0.5, .5, .5, .5);
	prisma(63, 5, 0.5, .5, .5, .5);
	prisma(63.5, 5, 0.5, .5, .5, .5);
	prisma(64, 5, 0.5, .5, .5, .5);
	prisma(64.5, 5, 0.5, .5, .5, .5);
	prisma(65, 5, 0.5, .5, .5, .5);
	prisma(65.5, 5, 0.5, .5, .5, .5);
	prisma(66, 5, 0.5, .5, .5, .5);
	prisma(66.5, 5, 0.5, .5, .5, .5);
	prisma(67, 5, 0.5, .5, .5, .5);
	prisma(67.5, 5, 0.5, .5, .5, .5);
	prisma(68, 5, 0.5, .5, .5, .5);
	prisma(68.5, 5, 0.5, .5, .5, .5);
	prisma(69, 5, 0.5, .5, .5, .5);
	prisma(69.5, 5, 0.5, .5, .5, .5);
	prisma(70, 5, 0.5, .5, .5, .5);
	prisma(70.5, 5, .5, .5, .5, .5);
	prisma(71, 5, 0.5, .5, .5, .5);
	prisma(71.5, 5, 0.5, .5, .5, .5);
	prisma(72, 5, 0.5, .5, .5, .5);
	prisma(72.5, 5, 0.5, .5, .5, .5);
	prisma(73, 5, 0.5, .5, .5, .5);
	prisma(73.5, 5, 0.5, .5, .5, .5);
	prisma(74, 5, 0.5, .5, .5, .5);
	prisma(74.5, 5, 0.5, .5, .5, .5);
	prisma(75, 5, 0.5, .5, .5, .5);
	prisma(75.5, 5, 0.5, .5, .5, .5);
	prisma(76, 5, 0.5, .5, .5, .5);
	prisma(76.5, 5, 0.5, .5, .5, .5);
	prisma(77.5, 5, 0.5, .5, .5, .5);
	prisma(77, 5, 0.5, .5, .5, .5);
	prisma(78.5, 5, 0.5, .5, .5, .5);
	prisma(78, 5, 0.5, .5, .5, .5);
	prisma(79, 5, 0.5, .5, .5, .5);
	prisma(79.5, 5, 0.5, .5, .5, .5);
	prisma(80, 5, 0.5, .5, .5, .5);
	prisma(80.5, 5, 0.5, .5, .5, .5);
	prisma(81, 5, 0.5, .5, .5, .5);
	prisma(81.5, 5, 0.5, .5, .5, .5);
	prisma(82, 5, 0.5, .5, .5, .5);
	prisma(82.5, 5, 0.5, .5, .5, .5);
	prisma(83, 5, 0.5, .5, .5, .5);
	prisma(83.5, 5, 0.5, .5, .5, .5);
	prisma(84, 5, 0.5, .5, .5, .5);
	prisma(84.5, 5, 0.5, .5, .5, .5);
	prisma(85, 5, 0.5, .5, .5, .5);
	prisma(85.5, 5, 0.5, .5, .5, .5);
	prisma(86, 5, 0.5, .5, .5, .5);
	prisma(86.5, 5, 0.5, .5, .5, .5);
	prisma(87, 5, 0.5, .5, .5, .5);
	prisma(87.5, 5, 0.5, .5, .5, .5);
	prisma(88, 5, 0.5, .5, .5, .5);
	prisma(88.5, 5, 0.5, .5, .5, .5);
	prisma(89, 5, 0.5, .5, .5, .5);
	prisma(89.5, 5, 0.5, .5, .5, .5);
	//CUARTA ESCALERA
	cubonormal(52.5, 5, 1, .5, .5, .5);
	cubonormal(53, 5, 1, .5, .5, .5);
	cubonormal(53.5, 5, 1, .5, .5, .5);
	cubonormal(54, 5, 1, .5, .5, .5);
	cubonormal(52.5, 5, 1.5, .5, .5, .5);
	cubonormal(53, 5, 1.5, .5, .5, .5);
	cubonormal(53.5, 5, 1.5, .5, .5, .5);
	cubonormal(52.5, 5, 2, .5, .5, .5);
	cubonormal(53, 5, 2, .5, .5, .5);
	cubonormal(52.5, 5, 2.5, .5, .5, .5);
	//primer tubo
	tubo(57, 5, 1, 1, .5, 1);
	//segundo tubo
	tubo(64.5, 5, 1, 1, .5, 1);
	//cubo normal
	cubonormal(60, 5, 2.5, .5, .5, .5);
	cubonormal(60.5, 5, 2.5, .5, .5, .5);
	cubonormal(61.5, 5, 2.5, .5, .5, .5);
	//cubo sorpresa
	cubosorpresa(61, 5, 2.5, .5, .5, .5);
	//ultima escalera final
	cubonormal(65, 5, 1, .5, .5, .5);
	cubonormal(65.5, 5, 1, .5, .5, .5);
	cubonormal(66, 5, 1, .5, .5, .5);
	cubonormal(66.5, 5, 1, .5, .5, .5);
	cubonormal(67, 5, 1, .5, .5, .5);
	cubonormal(67.5, 5, 1, .5, .5, .5);
	cubonormal(68.5, 5, 1, .5, .5, .5);
	cubonormal(68, 5, 1, .5, .5, .5);
	cubonormal(69, 5, 1, .5, .5, .5);//
	cubonormal(66, 5, 1.5, .5, .5, .5);
	cubonormal(66.5, 5, 1.5, .5, .5, .5);
	cubonormal(67, 5, 1.5, .5, .5, .5);
	cubonormal(67.5, 5, 1.5, .5, .5, .5);
	cubonormal(68.5, 5, 1.5, .5, .5, .5);
	cubonormal(68, 5, 1.5, .5, .5, .5);
	cubonormal(69, 5, 1.5, .5, .5, .5);//
	cubonormal(66, 5, 2, .5, .5, .5);
	cubonormal(66.5, 5, 2, .5, .5, .5);
	cubonormal(67, 5, 2, .5, .5, .5);
	cubonormal(67.5, 5, 2, .5, .5, .5);
	cubonormal(68.5, 5, 2, .5, .5, .5);
	cubonormal(68, 5, 2, .5, .5, .5);
	cubonormal(69, 5, 2, .5, .5, .5);//
	cubonormal(66.5, 5, 2.5, .5, .5, .5);
	cubonormal(67, 5, 2.5, .5, .5, .5);
	cubonormal(67.5, 5, 2.5, .5, .5, .5);
	cubonormal(68.5, 5, 2.5, .5, .5, .5);
	cubonormal(68, 5, 2.5, .5, .5, .5);
	cubonormal(69, 5, 2.5, .5, .5, .5);//
	cubonormal(67, 5, 3, .5, .5, .5);
	cubonormal(67.5, 5, 3, .5, .5, .5);
	cubonormal(68.5, 5, 3, .5, .5, .5);
	cubonormal(68, 5, 3, .5, .5, .5);
	cubonormal(69, 5, 3, .5, .5, .5);//
	cubonormal(67.5, 5, 3.5, .5, .5, .5);
	cubonormal(68.5, 5, 3.5, .5, .5, .5);
	cubonormal(68, 5, 3.5, .5, .5, .5);
	cubonormal(69, 5, 3.5, .5, .5, .5);//
	cubonormal(68.5, 5, 4, .5, .5, .5);
	cubonormal(68, 5, 4, .5, .5, .5);
	cubonormal(69, 5, 4, .5, .5, .5);//
	cubonormal(68.5, 5, 4.5, .5, .5, .5);
	cubonormal(69, 5, 4.5, .5, .5, .5);
	//ULTIMO CUBO
	cubonormal(73.5, 5, 1, .5, .5, .5);
	//HASTA
	cubonormal(73.7, 5, 1, .2, .2, 6);
	//CASTILLO
	castillo(76, 5, 1, 4, .5, 3);




	glPopMatrix();
	glutSwapBuffers();
}




void idle() {
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	llenarMatriz();
	//linea(-60*10+600,-40*10+400,5*10+600,40*10+400);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	//glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glutInitWindowSize(800, 700);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("MARIO BROS");
	initRendering();

	//init();
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutSpecialFunc(Arrowkeys);
	glutMouseFunc(onMouse);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}


